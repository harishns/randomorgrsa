import urllib2


def getPrimes(n, max, min=0):
    if checkquota() < 1:
        raise Exception, "Your www.random.org quota has already run out."
    requestparam = build_request(n, min, max)
    request = urllib2.Request(requestparam)
    request.add_header('User-Agent', 'randomwrapper/0.1 test alpha')
    opener = urllib2.build_opener()
    numlist = opener.open(request).read()
    return [int(n) for n in numlist.split()]


def build_request(howmany, min, max):
    randomorg = 'http://www.random.org/integers/?num='
    base_params = '&col=1&base=10&format=plain&rnd=new'
    params = str(howmany)+ '&min=' + str(min) + '&max=' + str(max)
    return randomorg + params + base_params

def checkquota():
    request = urllib2.Request("http://www.random.org/quota/?format=plain")
    request.add_header('User-Agent', 'randomwrapper/0.1 test alpha')
    opener = urllib2.build_opener()
    quota = opener.open(request).read()
    return int(quota)
    
    
    