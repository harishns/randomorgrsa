from rsa import generate_keypair, is_prime
from randomorgwrapper import getPrimes

def get_two_primes(nums):
    primes = []
    for n in nums:
        if len(primes) ==2:
            break
        if is_prime(n):
            primes.append(n)
    return primes[0], primes[1]

if __name__ == '__main__':

    print "RSA Public/Private Key"
    nums = getPrimes(20,999,11)
    p, q = get_two_primes(nums)
    print("primes : ", p,q)
    print "Generating your public/private keypairs now . . ."
    public, private = generate_keypair(p, q)
    print "Your public key is ", public ," and your private key is ", private